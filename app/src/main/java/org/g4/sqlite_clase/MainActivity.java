package org.g4.sqlite_clase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText textCode, textName, textSalary;
    private final String dbName = "myBase";
    private final String tableName = "customers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textCode = findViewById(R.id.text_cedula);
        textName = findViewById(R.id.text_name);
        textSalary = findViewById(R.id.text_salary);

        Button btnRegister = findViewById(R.id.btn_register);
        Button btnDelete = findViewById(R.id.btn_delete);
        Button btnModify = findViewById(R.id.btn_modify);
        Button btnSearch = findViewById(R.id.btn_search);

        btnRegister.setOnClickListener(this::registerCustomer);
        btnDelete.setOnClickListener(this::deleteCustomer);
        btnModify.setOnClickListener(this::modifyCustomer);
        btnSearch.setOnClickListener(this::searchCustomer);
    }


    private void registerCustomer(View view) {
        AdminSqliteOpenHelper admin = getSqliteOpenHelper();
        SQLiteDatabase database = admin.getWritableDatabase();

        String code = textCode.getText().toString();
        String name = textName.getText().toString();
        String salary = textSalary.getText().toString();

        if (code.isEmpty() || name.isEmpty() || salary.isEmpty()) {
            showMissingFieldsToFillToast();
            return;
        }

        ContentValues contentValues = mapToContentValues(code, name, salary);

        try {
            database.insert(tableName, null, contentValues);
        } catch (SQLiteConstraintException e) {
            showCustomerAlreadyExistsToast();
        }

        database.close();
        clearAllEditTexts();;

        showCustomerCreatedSuccessfullyToast();
    }

    private void deleteCustomer(View view) {
        AdminSqliteOpenHelper admin = getSqliteOpenHelper();
        SQLiteDatabase database = admin.getWritableDatabase();
        String code = textCode.getText().toString();

        if (code.isEmpty()) {
            showEnterCustomerCodeToast();
            return;
        }

        int nCustomersDeleted = database.delete(tableName, "code=" + code, null);
        database.close();
        clearAllEditTexts();

        if (nCustomersDeleted <= 0) {
            showCustomerNotFoundToast();
            return;
        }

        showCustomerDeletedSuccessfullyToast();
    }

    private void searchCustomer(View view) {
        AdminSqliteOpenHelper admin = getSqliteOpenHelper();
        SQLiteDatabase database = admin.getReadableDatabase();
        String code = textCode.getText().toString();

        if (code.isEmpty()) {
            showEnterCustomerCodeToast();
            return;
        }

        String query = "select name, salary from customers where code='" + code + "'";
        Cursor cursor = database.rawQuery(query, null);
        if (!cursor.moveToFirst()) {
            showCustomerNotFoundToast();
            database.close();
            return;
        }

        textName.setText(cursor.getString(0));
        textSalary.setText(cursor.getString(1));
        database.close();
        cursor.close();
    }

    private void modifyCustomer(View view) {
        AdminSqliteOpenHelper admin = getSqliteOpenHelper();
        SQLiteDatabase database = admin.getWritableDatabase();
        String code = textCode.getText().toString();
        String name = textName.getText().toString();
        String salary = textSalary.getText().toString();

        if (code.isEmpty() || name.isEmpty() || salary.isEmpty()) {
            showMissingFieldsToFillToast();
            return;
        }

        ContentValues contentValues = mapToContentValues(name, salary);

        int nCustomersUpdated = database.update(tableName, contentValues, "code=" + code, null);
        database.close();

        if (nCustomersUpdated <= 0) {
            showCustomerNotFoundToast();
            return;
        }

        clearAllEditTexts();
        showCustomerModifiedSuccessfullyToast();
    }

    private AdminSqliteOpenHelper getSqliteOpenHelper() {
        return new AdminSqliteOpenHelper(this, dbName, null, 1);
    }

    private void clearAllEditTexts() {
        textCode.setText("");
        textSalary.setText("");
        textName.setText("");
    }

    private ContentValues mapToContentValues(String code, String name, String salary) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("code", code);
        contentValues.put("name", name);
        contentValues.put("salary", salary);

        return contentValues;
    }

//    sobrecarga de métodos
    private ContentValues mapToContentValues(String name, String salary) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("salary", salary);

        return contentValues;
    }

    private void showCustomerNotFoundToast() {
        Toast.makeText(this, "El cliente no existe", Toast.LENGTH_SHORT).show();
    }

    private void showCustomerAlreadyExistsToast() {
        Toast.makeText(this, "Ya existe un usuario con esta cédula", Toast.LENGTH_SHORT).show();
    }

    private void showCustomerDeletedSuccessfullyToast() {
        Toast.makeText(this, "Cliente inactivado con éxito", Toast.LENGTH_SHORT).show();
    }

    private void showCustomerModifiedSuccessfullyToast() {
        Toast.makeText(this, "Cliente modificado con éxito", Toast.LENGTH_SHORT).show();
    }

    private void showCustomerCreatedSuccessfullyToast() {
        Toast.makeText(this, "Cliente creado con éxito", Toast.LENGTH_SHORT).show();
    }

    private void showMissingFieldsToFillToast() {
        Toast.makeText(this, "Faltan campos por llenar", Toast.LENGTH_SHORT).show();
    }

    private void showEnterCustomerCodeToast() {
        Toast.makeText(this, "Ingrese el código para buscar al cliente", Toast.LENGTH_SHORT).show();
    }
}
